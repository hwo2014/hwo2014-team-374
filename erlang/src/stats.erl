-module(stats).
-export([init/0, cumulate/2]).

-include("race.hrl").

-define(STAT_MAX, math:pow(2, 32)).
-define(STAT_MIN, -?STAT_MAX).

init() ->
	#stats{min = ?STAT_MAX, max=?STAT_MIN, sum = 0, count = 0, avg = 0}.

cumulate(Stats, Value) ->
	Min = erlang:min(Stats#stats.min, Value),
	Max = erlang:max(Stats#stats.max, Value),
	Sum = Stats#stats.sum + Value,
	Count = Stats#stats.count + 1,
	Avg = Stats#stats.sum / Count, 
	Stats#stats{ min = Min, max = Max, sum = Sum, count = Count, avg = Avg}.