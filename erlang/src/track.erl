-module(track).
-export([init/3]).
-include("race.hrl").

init(Status, TrackId, PieceData) ->
	ReadPieceFormat = fun (PieceStruct)  -> 
		case PieceStruct of 
			{struct, [{<<"length">>, Length}]} ->
				init_piece(1, 1, false, Length);
			{struct, [{<<"length">>, Length}, {<<"switch">>, Switch}]} ->
				init_piece(1, 1, Switch, Length);
			{struct, [{<<"radius">>, Radius}, {<<"angle">>, Angle}]} ->
				init_piece(Radius, Angle, false, 0);
			{struct, [{<<"radius">>, Radius}, {<<"angle">>, Angle}, {<<"switch">>, Switch}]} ->
				init_piece(Radius, Angle, Switch, 0);
			_ ->
				not_piece
		end
	end,

	Pieces = lists:map(ReadPieceFormat, PieceData),
	FilteredPieces = lists:filter(fun(Piece) -> Piece =/= not_piece end, Pieces),

	ApplyGainOfNext = fun (Piece, Acc) ->
		case Acc of
			{PreviousPiece, AdjustedPieces} ->
				TotalGain = (Piece#piece.gain / 2 + PreviousPiece#piece.gain * 2),
				AdjustedPiece = Piece#piece{gain = TotalGain},
				{Piece, [ AdjustedPiece | AdjustedPieces ]}	
		end
	end,

	LastPiece = lists:last(FilteredPieces),

	{_Gain, AdjustedPieces} = lists:foldl( ApplyGainOfNext, { LastPiece, []}, lists:reverse(FilteredPieces)),

	Track = #track{pieces = AdjustedPieces, id = TrackId},
	NewStatus = Status#status{track=Track},
	{NewStatus, message:ping()}.

init_piece(Radius, Angle, Switch, Length) ->
	Gain =  ( erlang:abs(Angle) / Radius) - 1,
	#piece{radius = Radius, angle = Angle, length = Length, switch = Switch, gain = Gain}.