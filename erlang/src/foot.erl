-module(foot).
-export([throttle/1]).
-include("race.hrl").

throttle(Status) ->

	% keimola 0.72 0.88 88 9317
	% germany 0.56 0.6 75 12516

	BaseThrottle = 0.7,
	MaxGain = 0.5,
	GainMultiplier = 0.7,
	MaxModifier = 0.4,
	AngleRange = 90,

	Car = Status#status.car,

	Angle = Car#car.angle, 
	Gain = Car#car.gain,

	RelativeAngle = erlang:abs(Angle) / AngleRange,
    ThrottleModifier = RelativeAngle * MaxModifier,

    LimitedGain = erlang:min(erlang:max(Gain, -MaxGain * GainMultiplier),0),

    NewThrottle = erlang:max(erlang:min(BaseThrottle - ThrottleModifier + LimitedGain,1),0),
    message:throttle(NewThrottle).