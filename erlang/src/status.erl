-module(status).
-export([init/0, init/2]).
-include("race.hrl").

init() ->
    #status{}.

init(Name, Color) ->
	Status = init(),
    Status#status{car=car:init(Name, Color)}.