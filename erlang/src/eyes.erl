-module(eyes).
-export([assess_status/2]).
-include("race.hrl").

assess_status(Status, CarData) ->

 	MyCar = Status#status.car, 

    [{MyAngle, MyPosition} | _OtherCarMovement]  = [ 
        {Angle, {PieceIndex, InPieceDistance}} || {struct, [
	            { <<"id">>, 
	                {struct, [{ <<"name">>, Name }, {<<"color">>, Color}]}
	            },
	            {<<"angle">>, Angle},
	            {<<"piecePosition">>, {struct, [
	            	{<<"pieceIndex">>, PieceIndex},
	            	{<<"inPieceDistance">>, InPieceDistance},
	            	{<<"lane">>, _Lane},
	            	{<<"lap">>, _Lap}
	            ]}}
	        ]} 
        <- CarData, Name == MyCar#car.name, Color == MyCar#car.color
    ],
    
    car:move(Status, MyAngle, MyPosition).