-module(message).
-export([join/2, joinRace/2, throttle/1, ping/0]).

%% Messages we send to server
join(BotName, Key) ->
    mochijson2:encode(
    	{struct, [
    		{msgType, <<"join">>},
            {data, {struct, [
                {name, list_to_binary(BotName)},
                {key, list_to_binary(Key)}
            ]}}
        ]}
    ).

joinRace(BotName, Key) ->
    mochijson2:encode(
        {struct, [
            {msgType, <<"joinRace">>},
            {data, {struct, [
                {botId, {struct, [
                    {name, list_to_binary(BotName)},
                    {key, list_to_binary(Key)}
                ]}},
                {trackName, <<"germany">>}
            ]}}
        ]}
    ).

throttle(Speed) ->
    mochijson2:encode(
    	{struct, [
           {<<"msgType">>, <<"throttle">>},
           {<<"data">>, Speed}
        ]}
    ).

ping() ->
    mochijson2:encode({struct, [{<<"msgType">>, <<"ping">>}]}).

