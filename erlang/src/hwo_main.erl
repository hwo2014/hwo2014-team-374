-module(hwo_main).
-export([main/0]).
-include("race.hrl").

main() ->
    [Host, PortStr, BotName, Key] = init:get_plain_arguments(),
    {Port, []} = string:to_integer(PortStr),
    io:fwrite("Starting bot ~p (connecting to ~s:~B)\n", [BotName, Host, Port]),
    hwo_lines:start(Host, Port),
    Status = status:init(),
    loop(BotName, Key, Status).

%% Inbound message handling

onMessage(Pid, Received, PreviousStatus) ->
    
    {NewStatus, Message} = case Received of
        {struct, [
            {<<"msgType">>, MessageType}, 
            {<<"data">>, Data}
        ]} ->
            just_ping("Game not running", [MessageType, Data], PreviousStatus);
        {struct, [
            {<<"msgType">>, <<"yourCar">>}, 
            {<<"data">>, {struct, [
                {<<"name">>, Name}, 
                {<<"color">>, Color}
            ]}},
            {<<"gameId">>, _GameId}
        ]} ->
            driver:enter_car(Name, Color);
        {struct, [
            {<<"msgType">>, MessageType},
            {<<"data">>, GameData},
            {<<"gameId">>, _GameId}
        ]} ->
            run_game(PreviousStatus, MessageType, GameData);  
        {struct, [
            {<<"msgType">>, MessageType},
            {<<"data">>, GameData},
            {<<"gameId">>, _GameId},
            {<<"gameTick">>, _GameTick}
        ]} ->
            run_game(PreviousStatus, MessageType, GameData);
        _ ->
            just_ping(Received, PreviousStatus)
    end,
    hwo_lines:send(Pid, Message),  
    NewStatus.               

just_ping(Value, PreviousStatus) ->
    io:fwrite("Unhandled message ~p\n", [Value]),  
    {PreviousStatus, message:ping()}.
just_ping(Label, Value, PreviousStatus) ->
    io:fwrite("~p ~p\n", [Label, Value]),  
    {PreviousStatus, message:ping()}.

run_game(Status, MessageType, Data) ->
    case MessageType of 
        <<"gameInit">> ->
            case Data of 
                {struct, [
                    {<<"race">>, {struct, [
                        {<<"track">>, {struct, [
                            {<<"id">>, TrackId},
                            {<<"name">>, TrackName},
                            {<<"pieces">>, PieceData},
                            {<<"lanes">>, _LaneData},
                            {<<"startingPoint">>, _Start}
                        ]}},
                        {<<"cars">>, _CarData},
                        {<<"raceSession">>, _SessionData}
                    ]}}
                ]} ->
                    io:fwrite("Welcome to ~p\n", [TrackName]),  
                    track:init(Status, TrackId, PieceData);
                _ ->
                    just_ping("Information", [MessageType, Data], Status)
            end;
        <<"carPositions">> ->
            driver:drive(Status, Data);   
        <<"lapFinished">> -> 
            case Data of
                {struct, [
                    {<<"car">>, {struct, [
                        {<<"name">>, Name},
                        {<<"color">>, Color}
                    ]}},
                    {<<"lapTime">>, {struct, [
                        {<<"lap">>, LapNumber},
                        {<<"ticks">>, Ticks},
                        {<<"millis">>, Millis}
                    ]}},
                    _RaceTime,
                    _Ranking
                ]} when Name =:= Status#status.car#car.name, Color =:= Status#status.car#car.color ->
                    just_ping("New lap", [LapNumber, Ticks, Millis, Status#status.car#car.gainStats], Status);
                _ ->
                    just_ping("Other player lap", Data, Status)
            end;
        <<"gameEnd">> ->
            just_ping("Race ended, lets analyze stats:", [[Status#status.car#car.angleStats, Status#status.car#car.gainStats]], Status);  
        _ ->
            just_ping("Information", [MessageType, Data], Status)
    end.   
                   

%% Main loop. hwo_lines is responsible for sending us parsed lines.

loop(BotName, Key, Status) ->
    receive 
        {hwo_line_connected, Pid} ->
            hwo_lines:send(Pid, message:joinRace(BotName, Key)),
            NewStatus = status:init(),
            loop(BotName, Key, NewStatus);
        {hwo_line, Pid, Line} ->
            Msg = mochijson2:decode(Line),
            NewStatus = onMessage(Pid, Msg, Status),
            loop(BotName, Key, NewStatus);
        hwo_line_closed ->
            io:fwrite("closed, exiting\n");
        {'EXIT', _, _} ->
            io:fwrite("IO process has died. A write error, likely. Must exit.\n");
        Msg ->
            io:fwrite("Error, exiting hwo_main: unknown msg: ~p\n", [Msg])
    end.

