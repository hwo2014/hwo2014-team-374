-module(driver).
-export([enter_car/2, drive/2]).
-include("race.hrl").

enter_car(Name, Color) ->
    NewStatus = status:init(Name, Color),
    io:fwrite("Driving car ~p\n", [[Name, Color]]),
    {NewStatus, message:ping()}.

drive(Status, PositionData) ->
	NewStatus = eyes:assess_status(Status, PositionData),
	Message = foot:throttle(NewStatus), 
	{NewStatus, Message}.  
