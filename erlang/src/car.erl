-module(car).
-export([init/2,move/3]).
-include("race.hrl").

init(Name, Color) ->
	#car{name = Name, color = Color, angleStats=stats:init(), gainStats=stats:init()}.

move(Status, Angle, Position) ->
	Car = Status#status.car,
	
	{PiecePosition, _InPiecePosition} = Position,

	Track = Status#status.track,
	Pieces = Track#track.pieces,

	Piece = lists:nth(PiecePosition + 1, Pieces),
	Gain = Piece#piece.gain,

	AngleStats = Car#car.angleStats,
	CumulatedAngleStats = stats:cumulate(AngleStats, Angle),

	GainStats = Car#car.gainStats,
	CumulatedGainStats = stats:cumulate(GainStats, Gain),

	MovedCar = Car#car{
		angle=Angle, 
		angleStats=CumulatedAngleStats,
		position=Position, 
		gain = Gain, 
		gainStats = CumulatedGainStats
	},
	Status#status{car = MovedCar}.
